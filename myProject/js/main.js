document.getElementById("hello_text").textContent = "初めてのjavascript";

var count = 0;
var cells;

//ブロックのパターン
var blocks = {
    i:{
        class:"i",
        pattern:[
            [1,1,1,1]
        ]
    },
    o:{
        class:"o",
        pattern:[
            [1,1],
            [1,1]
        ]
    },
    t:{
        class:"t",
        pattern:[
            [0,1,0],
            [1,1,1]
        ]
    },
    s:{
        class:"s",
        pattern:[
            [0,1,1],
            [1,1,0]
        ]
    },
    z:{
        class:"z",
        pattern:[
            [1,1,0],
            [0,1,1]
        ]
    },
    j:{
        class:"j",
        pattern:[
            [1,0,0],
            [1,1,1]
        ]
    },
    l:{
        class:"l",
        pattern:[
            [0,0,1],
            [1,1,1]
        ]
    },
}

loadTable();

document.addEventListener("keydown", onKeyDown);

function onKeyDown(event){
    if(event.keyCode === 37){
        moveLeft();
    }else if(event.keyCode === 39){
        moveRight();
    }
}

//setInterval(動かしたい関数, 繰り返す間隔(ms));
setInterval(function(){
    count++;
    document.getElementById("hello_text").textContent = "初めてのjs("+count+")";
    

    //ゲームオーバー判定
    if(!isFalling){
        for(var row = 0; row < 1; row++){
            for(var col = 0; col < 10; col++){
                if(cells[row][col].className !== ""){
                    alert("game over");
                }
            }
        }
    }

    if(hasFallingBlock()){
        fallBlocks();
    }else{
        deleteRow();
        generateBlock();
    }
},1000);

//ゲーム盤を読み込む
function loadTable(){
    //td内の要素を取得
    var td_array = document.getElementsByTagName("td");
    //td_arrayを二次元配列に変更
    cells = [];
    var index = 0;
    for(var row=0; row<20; row++){
        //各行を二次元に変換
        cells[row] = [];
        for(var col=0; col<10; col++){
            cells[row][col] = td_array[index];
            index++;
        }
    }

}

//落下させる関数
function fallBlocks(){
    //底に付いていないか確認
    for(var col = 0; col < 10; col++){
        if(cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;//底についているので落とさない
            return;
        }    
    }
    //１マス下に他のブロックがないか確認
    for(var row = 18; row >= 0; row--){
        for(var col = 0; col < 10; col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
                    isFalling = false;//下にブロックがいるので落とさない
                    return;
                }
            }
        }
    }


    //１マス下げる作業
    //下から二行目から順に下げていく
    for(var row = 18; row >= 0; row--){
        for(var col = 0; col < 10; col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;
function hasFallingBlock() {
    // 落下中のブロックがあるか確認する
    return isFalling;
}
  
function deleteRow() {
    // そろっている行を消す
    for(var row = 19; row >= 0; row--){
        var canDelete = true;
        for(var col = 0; col < 10; col++){
            if(cells[row][col].className === ""){
                canDelete = false;
            }
        }
        if(canDelete){
            for(var col = 0; col < 10; col++){
                cells[row][col].className = "";
            }
            //全てのブロックを１マス下に移動
            for(var downRow = row - 1; downRow >= 0; downRow--){
                for(var col = 0; col < 10; col++){
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}
  
var fallingBlockNum = 0;
function generateBlock() {
    // ランダムにブロックを生成する
    //ブロックをランダムに一つ決定
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    //ブロックの番号
    var nextFallingBlockNum = fallingBlockNum + 1;

    //ブロックの配置
    var pattern = nextBlock.pattern;
    for(var row = 0; row < pattern.length; row++){
        for(var col = 0; col < pattern[row].length; col++){
            if(pattern[row][col]){
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    //生成したブロックが落下するためブロック生成を止める
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}
  
function moveRight() {
    // ブロックを右に移動させる
    for(var row = 0; row < 20; row++){
        for(var col = 9; col >= 0; col--){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
  
function moveLeft() {
    // ブロックを左に移動させる
    for(var row = 0; row < 20; row++){
        for(var col = 0; col < 10; col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

